# JETA

```
Sa e dua giithe jeten!
Se atje gienj te verteten,
Yjte, henen, haPesiren,
Te mugetit, naten, diten,
Mengjeze, djelline, driren,
Mbremen dh' ate erreciren;
Naten, kur esht' e qetuar
Edhe gjesendi s'degionet;
Qiellin kur esht' i edruar
E hapesira ndritonet,
A kur hapen here-here
Ret' e zeza sten1 nd'ere;
Vapen, vjeshten, dimrin, veren,
Rete, shine dhe lumenjte,
Gjemim, mjergulle, Perrenjte,
Deoren,
bresherin' e eren'
Malet, fushat edhe brigjet,
Grykat e gukat e shtigjet.
Oshetim6en, Pyjet,
druret'
Shkembenjte, gerxhet' e guret,
Lulet, barerat qe mbijne,
E shpeste qe fluturojne
Edhe kengera kendojne
E neper dega levrijne.
Dhe njerin' e bagetine
Dhe giithe E'eshte
ne
jete
I dua si Perendine,
Se ngado qe kthenj syte,
Shoh atje Zotn' e vertete,
Q'eshte nje e s'ka te dyte.
```