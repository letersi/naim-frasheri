# titull
## titull me i vogel
### akoma me i vogel
#### akoma me i vogel

```
formatim identik
```

**bold**, **grassetto**, **shqip nuk me kujtohet si i thone**

_italic_  _forma e shkrimit italic_

[Tksti i paraqitur](link)

lista te renditura:
1. objekt1
2. objekt2
3. objekt3
 ose
1. objekt1
   1. nen objekt1
   2. nen objekt2
   3. nen objekt3
2. objekt2

lista te pa renditura:
- objekti1
- objekti2
- objekti3
ose 
- objekti1
  - nen objekti1
  - nen objekti2
- objekti2

per te ndar listat:
- lista1
^
- lista2
^
- lista3

imazhe:
![pershkrim i mazhit](/images/path/to/folder/image.png "titulli")

video:

video nga youtube:
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/enMumwvLAug" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

video nga kartela locale (html5):
<!-- blank line -->
<figure class="video_container">
  <video controls="true" allowfullscreen="true" poster="path/to/poster_image.png">
    <source src="path/to/video.mp4" type="video/mp4">
    <source src="path/to/video.ogg" type="video/ogg">
    <source src="path/to/video.webm" type="video/webm">
  </video>
</figure>
<!-- blank line -->

