# VARFËRIA

```
Ju, të pasur, kur rrini
Rreth zjarrit me gëzime,
Qeshni, hani e pini,
Xbaviti me dëfrime,
Kërceni e këndoni
Dhe çeli e gëzoni,
Bëni gaxhi e loni
Edhe prëhi e ngrohi.
U vjen së largu afshi
Ndë sallonet të gjerë,
Shtruarë pej mëndafshi,
Mbushur me gjë të vlerë.
Nga ari, nga ergjëndi,
Nga floriri, nga drita
Llapëtin gjithë vëndi,
Tfaqet natënë dita.

Zonjatë bukuroshe
Veshur me rrobat’ e arta,
Vashatë vogëlushe,
Të bardha posi karta
Djemthit’ e përkëdhelur,
Të larë e të ndruar,
Me zëmërë të çelur,
Me buzë të gëzuar,
Vinë rreth e kërcejnë
Dhe qeshin e këndojnë,
Lozinë e dëfrjnë,
Kohën me gas e shkojnë,
Dhe juve, duke parë
Me sy fatbardhësinë,
S’u vjen fare të qarë
Vallë për varfërinë?
E dini q’është jashtë
Një plak’ e dobëtuar,
E përgjynjur, e lashtë,
E mjer’, e dëshëruar,
E fishkur’ edh’ e thatë,
E thinjur’ e kërcure,
E urtë dit’ e natë,
Dergjetë nër qivure!
Një plak q’i vdiq fëmija,
Edhe mbet i helmuar,
Se iu shkretua shtëpija,
Mbet si kandil i shuar!
Një foshnj’ e varfëruar
Pa nënëz’ e pa atë,
E gjor’ e dobëtuar,
E ligurë e e thatë!
Gjithë këta të mjerë
Në të këqia rrojnë,
Vin’ e lipin më derë,
Një kafshitë kërkojnë!
E kur vjen të kërkojnë,
Brengën s’munt’ ta rrëfenjë,
Po zë dhe të këndonjë
E rreh të na dëfrenjë.
Sa ‘shtë turp për njerinë,
Kur shkojm’ e s’kthejmë synë!
S’dëgjojmë Perëndinë?
S’është vëllathi ynë?
Oh, epni ju që kini!
Kush i ndih varfërisë,
Ay q’ep, po e dini
Q’i huan Perëndisë.
T’apin, se vjen një ditë
Q’e lemë këtë jetë
Dhe gjën’ e bagëtitë,
Vemi në të vërtetë,
Përpara Perëndisë,
Që do të na gjykonjë;
Urat’ e varfërisë
Arrin të na shpëtonjë,
Ajo or’ e borinjtë,
Që ligjëron e bije,
Rreth t’i plakin të rinjtë,
Buzë varrit na shpije.
Epni t’u falnjë Zoti
Juve gjithë të mirat
E të mos mutnjë moti
T’u sjellë të pahirat.
Mba unazat në dorë
Pa njëfarë nevojë!
Zonjë, shumë të gjorë
Vdesën për bukë ngoje!
Ep t’apë Perëndija
Të gjitha mirësitë,
Të të kenë fëmija
Shëndet e jet’ e ditë.
Zot i math’, i vërtetë
E gjithë ju të Mirët,
Mos e lini përjetë
Njerinë n’errësirët!
```