#!/bin/bash
### render all the formats

cd $(dirname $0)
rm -rf public/

Rscript -e "bookdown::render_book('index.Rmd', 'all', output_dir = 'public')"
